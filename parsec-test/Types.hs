module Types where

type Var = String

data Op = Plus
        | Minus
        | Mult
        | Div
        deriving (Eq,Show)

data Expr = Expr Op [Expr]
          | Var Var
          | Number Int
          deriving (Eq,Show)

data Operator = Print  Expr
              | Read   Var
              | Assign Var Expr
              deriving (Eq,Show)

newtype Program = Program [Operator] deriving (Eq,Show)

