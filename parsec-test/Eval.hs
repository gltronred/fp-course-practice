module Eval where

import Types
import Data.Either (partitionEithers)
import qualified Data.Map as M

operation Plus  = (+)
operation Minus = (-)
operation Mult  = (*)
operation Div   = div

evaluate :: M.Map Var Int -> Expr -> Either String Int
evaluate vars (Var name) =
  case M.lookup name vars of
    Nothing -> Left $ "Variable " ++ name ++ " was not initialized"
    Just value -> Right value
evaluate vars (Number x) = Right x
evaluate vars (Expr op exprs) = let
  (errors,results) = partitionEithers $ map (evaluate vars) exprs
  in if not $ null errors
     then Left $ "There were errors: " ++ concat errors
     else Right $ foldl1 (operation op) results

runOperator :: M.Map Var Int -> Operator -> IO (M.Map Var Int)
runOperator vars (Print expr) = do
  case evaluate vars expr of
    Left err -> putStrLn $ "Error: " ++ err
    Right res -> putStrLn $ show res
  return vars
runOperator vars (Read name) = do
  val <- readLn
  return $ M.insert name val vars
runOperator vars (Assign name expr) = do
  case evaluate vars expr of
    Left err -> error $ "Error: " ++ err
    Right res -> case M.lookup name vars of
      Nothing -> return $ M.insert name res vars
      Just val -> return $ M.adjust (const res) name vars

runProgram :: Program -> IO ()
runProgram (Program ops) = runProgram' M.empty ops

runProgram' vars [] = return ()
runProgram' vars (op:ops) = do
  vars' <- runOperator vars op
  runProgram' vars' ops

