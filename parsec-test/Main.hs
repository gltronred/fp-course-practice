
import Parser
import Eval
import qualified Data.ByteString.Char8 as BS
import System.Environment

main = do
  args <- getArgs
  let fname = args!!0
  ls <- BS.readFile fname
  case parseOnly program ls of
    Left err -> print err
    Right p -> runProgram p


