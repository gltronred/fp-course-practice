{-# LANGUAGE OverloadedStrings #-}

module Parser (program,parseOnly) where

import Types

import Data.Attoparsec
import Data.Attoparsec.ByteString.Char8
import Data.Maybe

{- program ::= operator ;
               operator ; program
   operator ::= print expr
                read var
                var := expr
   expr ::= pexpr
            pexpr + expr
            pexpr - expr
   pexpr ::= vexpr
             vexpr * pexpr
             vexpr / pexpr
   vexpr ::= var
             num
             (expr)
   var ::= [a-z]+
   num ::= [0-9]+
-}

{-
operator `sepBy` char ';'
===
sepBy operator (char ';')
-}

program :: Parser Program
program = operator `sepBy1` char ';' >>= return . Program

{-
do
  string "print "
  e <- expr
  return $ Print e

===

string "print " >> expr >>= return . Print

===

fmap Print $ string "print " >> expr

-}

operator :: Parser Operator
operator = choice [string "print " >> expr >>= return . Print
                  ,string "read " >> var' >>= return . Read
                  ,do
                    x <- var'
                    string ":="
                    e <- expr
                    return $ Assign x e]

var' :: Parser Var
var' = many1 letter_ascii

var :: Parser Expr
var = do
  xs <- var'
  return $ Var xs

num :: Parser Expr
num = do
  xs <- many1 digit
  return $ Number $ read xs

exprOp :: Parser Expr -> Char -> Op -> Parser Expr -> Parser Expr
exprOp part ch op whole = do
  p <- part
  char ch
  rest <- whole
  return $ Expr op [p,rest]

exprInBrackets = do
  char '('
  e <- expr
  char ')'
  return e

vexpr :: Parser Expr
vexpr = choice [var
               ,num
               ,exprInBrackets]

pexpr = choice [exprOp vexpr '*' Mult pexpr
               ,exprOp vexpr '/' Div pexpr
               ,vexpr]

expr :: Parser Expr
expr = choice [exprOp pexpr '+' Plus expr
              ,exprOp pexpr '-' Minus expr
              ,pexpr]

