
import Test.QuickCheck

sumNeg :: [Int] -> Int
sumNeg [] = 0
sumNeg (x:xs) = (if x<0 then x else 0) + sumNeg xs

prop_sumNeg xs = sum (filter (<0) xs) == sumNeg xs

test1 = quickCheck $ forAll arbitrary prop_sumNeg

---------------------------------------------------

data Expr = Number Int
          | Plus Expr Expr
          | Mult Expr Expr
          deriving (Show,Eq)

eval :: Expr -> Int
eval (Number x) = x
eval (Plus a b) = eval a + eval b
eval (Mult a b) = eval a * eval b

expand :: Expr -> Expr
expand expr = undefined

genTree :: Int -> Gen Expr
genTree n = if n == 0
                    then do
                      x <- choose (0,100)
                      return $ Number x
                    else do
                      n' <- choose (0,n-1)
                      let n'' = n-n'-1
                      f <- elements [Plus, Mult]
                      left <- genTree n'
                      right <- genTree n''
                      return $ f left right

gen = sized genTree

test2 = quickCheck $ forAll gen $ \expr -> eval (expand expr) == eval expr

