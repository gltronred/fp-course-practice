module Main where

import Data.Maybe (catMaybes, isJust, fromJust)
import Data.List (nub,transpose)

data Sudoku = Sudoku { unSudoku :: [[Maybe Int]] }
-- data Maybe a = Just a | Nothing

-- class Show a where
--   show :: a -> String

showElt Nothing = "- "
showElt (Just x)= show x ++ " "

showRow ys = concatMap showElt ys ++ "\n"

instance Show Sudoku where
  show (Sudoku xs) = concatMap showRow xs

{- | Update function for foldr
-}
updateItem :: Int -> Maybe Int -> (Bool,[Maybe Int]) -> (Bool,[Maybe Int])
updateItem x Nothing (False,xs) = (True,Just x:xs)
updateItem x (Just y) (False,xs) = (False,Just y:xs)
updateItem x z (True,xs) = (True,z:xs)

{- | Change Nothing in row to given x

>>> changeNothingRow 9 [Nothing,Nothing,Just 5]
Just [Nothing,Just 9,Just 5]

>>> changeNothingRow 9 [Nothing,Just 5]
Just [Just 9,Just 5]

>>> changeNothingRow 9 [Just 5]
Nothing
-}
changeNothingRow :: Int -> [Maybe Int] -> Maybe [Maybe Int]
changeNothingRow x row = let
  (wasNothing,row') = foldr (updateItem x) (False,[]) row
  in if wasNothing
     then Just row'
     else Nothing

changeNothing' :: Int -> [[Maybe Int]] -> (Bool,[[Maybe Int]])
changeNothing' x [] = (False,[])
changeNothing' x (row:rows) = let
  row' = changeNothingRow x row
  (changed,rows') = changeNothing' x rows
  in case row' of
    Just r -> (True,r:rows)
    Nothing -> (changed,row:rows')

changeNothing :: Int -> Sudoku -> Maybe Sudoku
changeNothing x sudoku = let
  rows = unSudoku sudoku
  (changed,rows') = changeNothing' x rows
  in if changed
     then Just $ Sudoku rows'
     else Nothing

generateAll :: Sudoku -> [Sudoku]
generateAll sudoku = let
  mnext = map (\x -> changeNothing x sudoku) [1..9]
  next = map fromJust mnext
  in if isJust $ mnext!!0
     then concatMap generateAll $ filter isValid next
     else [sudoku]

{- |
>>> noDup [Nothing,Nothing,Nothing]
True

>>> noDup [Just 1, Just 2, Just 3]
True

>>> noDup [Just 1, Just 2, Just 1]
False

>>> noDup [Just 1, Just 1, Nothing]
False
-}
noDup :: [Maybe Int] -> Bool
noDup xs = let
  nums = catMaybes xs
  in length nums == length (nub nums)

{- | Returns by columns
cols [[1, 2],
      [3, 4]] =
 [[1,3]
 ,[2,4]]

>>> (cols $ unSudoku sample3)!!0
[Just 4,Nothing,Nothing,Nothing,Nothing,Nothing,Nothing,Just 5,Just 1]
-}
cols :: [[Maybe Int]] -> [[Maybe Int]]
cols xs = transpose xs

{- | By 3 elements

>>> by3 [1,2,3,4,5,6]
[[1,2,3],[4,5,6]]
-}
by3 :: [a] -> [[a]]
by3 [] = []
by3 (x:y:z:xs) = [x,y,z]:by3 xs

{- | Rearrange boxes
-}
by9 :: [[a]] -> [[a]]
by9 [] = []
by9 (a:b:c:d:e:f:g:h:i:xs) = (a++d++g) :
                             (b++e++h) :
                             (c++f++i) :
                             by9 xs

{- | Return by squares

>>> (squares $ unSudoku sample6)!!0
[Just 5,Just 3,Nothing,Just 6,Nothing,Nothing,Nothing,Just 9,Just 8]

>>> (squares $ unSudoku sample6)!!3
[Just 8,Nothing,Nothing,Just 4,Nothing,Nothing,Just 7,Nothing,Nothing]
-}
squares :: [[Maybe Int]] -> [[Maybe Int]]
squares xs = by9 $ by3 $ concat xs

{- |
>>> isValid blank
True

>>> isValid sample1
True

>>> isValid sample4
True

>>> isValid invalid1
False

>>> isValid invalid2
False
-}
isValid :: Sudoku -> Bool
isValid (Sudoku xs) = all noDup xs &&
                      all noDup (cols xs) &&
                      all noDup (squares xs)

solve :: Sudoku -> Maybe Sudoku
solve sudoku = let
  variants = generateAll sudoku
  validSudokus = filter isValid variants
  solution = head validSudokus
  in if null validSudokus
     then Nothing
     else Just solution

main = case solve sample6 of
  Just sol -> print sol
  Nothing -> putStrLn "No solution"


o = -1

blank :: Sudoku
blank    = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o]]

sample1 :: Sudoku
sample1  = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[3, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o]]

sample2 :: Sudoku
sample2  = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[3, o, o, o, o, o, o, o, o],
            [o, 5, o, o, o, o, o, o, o],
            [o, o, 6, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o]]

sample3 :: Sudoku
sample3  = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[4, o, o, o, o, o, 8, o, 5],
            [o, 3, o, o, o, o, o, o, o],
            [o, o, o, 7, o, o, o, o, o],
            [o, 2, o, o, o, o, o, 6, o],
            [o, o, o, o, 8, o, 4, o, o],
            [o, o, o, o, 1, o, o, o, o],
            [o, o, o, 6, o, 3, o, 7, o],
            [5, o, o, 2, o, o, o, o, o],
            [1, o, 4, o, o, o, o, o, o]]

sample4 :: Sudoku
sample4  = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[4, 8, 3, 9, 2, 1, 6, 5, 7], 
            [9, 6, 7, 3, 4, 5, 8, 2, 1],
            [2, 5, 1, 8, 7, 6, 4, 9, 3],
            [5, 4, 8, 1, 3, 2, 9, 7, 6],
            [7, 2, 9, 5, 6, 4, 1, 3, 8],
            [1, 3, 6, 7, 9, 8, 2, 4, 5],
            [3, 7, 2, 6, 8, 9, 5, 1, 4],
            [8, 1, 4, 2, 5, 3, 7, 6, 9],
            [6, 9, 5, 4, 1, 7, 3, 8, 2]]

sample5 :: Sudoku
sample5  = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[4, 8, 3, 9, 2, 1, 6, 5, 7], 
            [9, 6, 7, 3, 4, 5, 8, 2, 1],
            [2, 5, o, o, 7, 6, 4, o, 3],
            [5, 4, 8, 1, 3, 2, 9, 7, 6],
            [7, 2, o, 5, 6, 4, 1, o, o],
            [1, 3, 6, 7, 9, 8, 2, 4, 5],
            [3, 7, o, 6, o, 9, 5, o, 4],
            [8, 1, 4, 2, 5, 3, 7, 6, 9],
            [6, 9, 5, 4, 1, 7, 3, 8, 2]]

sample6 :: Sudoku
sample6 = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
          [[5,3,o, o,7,o, o,o,o],
           [6,o,o, 1,9,5, o,o,o],
           [o,9,8, o,o,o, o,6,o],

           [8,o,o, o,6,o, o,o,3],
           [4,o,o, 8,o,3, o,o,1],
           [7,o,o, o,2,o, o,o,6],

           [o,6,o, o,o,o, 2,8,o],
           [o,o,o, 4,1,9, o,o,5],
           [o,o,o, o,8,o, o,7,9]]

sample7 :: Sudoku
sample7 = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
          [[o,o,6, 7,o,3, 5,o,o],
           [o,o,o, o,4,o, o,o,o],
           [5,o,o, o,o,o, o,o,2],

           [9,o,o, o,o,o, o,o,7],
           [o,3,o, o,o,o, o,4,o],
           [8,o,o, o,o,o, o,o,1],

           [1,o,o, o,o,o, o,o,4],
           [o,o,o, o,o,o, o,o,o],
           [o,5,9, 2,6,7, 3,1,o]]

invalid1 :: Sudoku
invalid1 = Sudoku $ map (map  (\x -> if x==o then Nothing else Just x))
           [[3, 3, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o]]

invalid2 :: Sudoku
invalid2 = Sudoku $ map (map (\x -> if x==o then Nothing else Just x))
           [[3, o, o, o, o, o, o, o, o],
            [o, 3, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o],
            [o, o, o, o, o, o, o, o, o]]


