module Grep where

import Text.Regex.Base
import Text.Regex.Posix

newtype Pattern = Pattern { unPattern :: Regex }

-- instance Show Pattern where
--   show (Pattern p) = show p

mkPattern :: String -> Pattern
mkPattern = Pattern . makeRegex

match :: Pattern -> String -> Bool
match (Pattern p) s = matchTest p s


