
import Grep

import Test.Tasty
import Test.Tasty.HUnit
import Test.HUnit

tests = testGroup "match"
        [ testCase "abc-abcdef" $ match (mkPattern "abc") "abcdef" @?= True
        , testCase "abc-dabcf"  $ match (mkPattern "abc") "dabcf"  @?= True
        , testCase "abc-deabc"  $ match (mkPattern "abc") "deabc"  @?= True
        , testCase "aaa-aaaa"   $ match (mkPattern "aaa") "aaaa"   @?= True
        , testCase "aaa-baaa"   $ match (mkPattern "aaa") "baaa"   @?= True
        , testCase "abc-abc"    $ match (mkPattern "abc") "abc"    @?= True
        , testCase "abc-def"    $ match (mkPattern "abc") "def"    @?= False
        , testCase "aaa-baa"    $ match (mkPattern "aaa") "baa"    @?= False
        , testCase "-abcdef"    $ match (mkPattern "")    "abcdef" @?= True
        , testCase "-"          $ match (mkPattern "")    ""       @?= True
        , testCase "abc-"       $ match (mkPattern "abc") ""       @?= False
        ]

main = defaultMain tests

