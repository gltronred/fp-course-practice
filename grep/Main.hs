
import Grep

import System.Environment
import System.IO

usage :: IO ()
usage = do
  putStrLn "Use mygrep with argument"
  putStrLn "Good luck!"

main :: IO ()
main = do
  args <- getArgs
  if length args == 0 || length args > 1
    then usage
    else do
      contents <- getContents
      let pattern = mkPattern $ head args
          lns = lines contents
          good = filter (match pattern) lns
      mapM_ putStrLn good
